Here is the database schema for the searches on both products. I also included a SQL join statement to give you an idea what we've been doing and how it all fits together.

In the end we just want a simple search that is fast and indexes basically everything to be searched on. the (1) Report search should be a relevance search with the most relevant content at the top. After the search is done, you should be able to sort the results by date instead of relevance. (2) EMT search should be a similar relevance search on all fields listed and after the search is complete there should be options to sort by f.round, p.state, p.country.

===============================
Report Search - search on a single table. meta_kewords field is a huge BLOB field
===============================

SELECT * FROM report WHERE (type == '1') AND (meta_keywords LIKE '%".$search_word."%' OR report_code LIKE '%".$search_word."%' OR title LIKE '%".$search_word."%' OR toc LIKE '%".$search_word."%' OR companies LIKE '%".$search_word."%'") ORDER BY published DESC, timestamp

Database schema:

report
-------------------
report_id
report_code
title
pages
exhibits
published
forecast
publisher
distributor
overview
toc
companies
brochure_url
price_single
price_corporate
meta_keywords
meta_description
type
timestamp

===============================
EMT Search - emt_company is the main table and all other tables associate to it via the emt_id field
===============================

Example query:

SELECT c.emt_id
, c.name
, c.description
, c.timestamp
, c.status
, l.market
, k.keyword
, f.financing_status
, f.round
, f.amount
, f.investors_in
, f.notes
, p.city
, p.state
, p.country
, p.notes

FROM emt_company AS c
INNER
JOIN emt_market AS m
ON m.emt_id = c.emt_id
INNER
JOIN market_list AS l
ON l.market_id = m.market_id
INNER
JOIN emt_keywords AS k
ON k.emt_id = c.emt_id
INNER
JOIN emt_funding AS f
ON f.emt_id = c.emt_id
INNER
JOIN emt_profile AS p
ON p.emt_id = c.emt_id
WHERE
, c.name LIKE '".$search_word."' OR
, c.description LIKE '".$search_word."' OR
, c.timestamp LIKE '".$search_word."' OR
, c.status LIKE '".$search_word."' OR
, l.market LIKE '".$search_word."' OR
, k.keyword LIKE '".$search_word."' OR
, f.financing_status LIKE '".$search_word."' OR
, f.round LIKE '".$search_word."' OR
, f.amount LIKE '".$search_word."' OR
, f.investors_in LIKE '".$search_word."' OR
, f.notes LIKE '".$search_word."' OR
, p.city LIKE '".$search_word."' OR
, p.state LIKE '".$search_word."' OR
, p.country LIKE '".$search_word."' OR
, p.notes LIKE '".$search_word."' OR

ORDER BY c.timestamp DESC

Database schema:

emt_company
------------------
emt_id
name
description
status
timestamp

emt_keywords
-----------------
emt_id
keyword

emt_market
-----------------
emt_id
market_id

emt_funding
-----------------
f_id
emt_id
financing_status
round
amount
investors_in
fiscal_quarter
fiscal_year
trial_fda
current_revenue
sales_projection
need_partners
need_partners_other
notes
status
timestamp

emt_profile
---------------------
emt_id
website
exec_salutation
exec_first_name
exec_last_name
exec_pro_designation
exec_title
exec_phone
exec_email
address1
address2
city
state
zip
country
phone
fax
year_established
notes
timestamp
status

I can provide you with the WHM login for our virtual dedicated server if youd like to begin installing/configuring Elastic search