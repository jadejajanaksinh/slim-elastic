<?php

require __DIR__ . '/vendor/autoload.php';
if (PHP_SAPI == 'cli') {
    $argv = $GLOBALS['argv'];
    array_shift($argv);

    $pathInfo       = implode('/', $argv);

    $env = \Slim\Http\Environment::mock(['REQUEST_URI' => '/' . $pathInfo]);


    $settings = require __DIR__ . '/src/settings.php'; // here are return ['settings'=>'']


    //I try adding here path_info but this is wrong, I'm sure
    $settings['environment'] = $env;

    $app = new \Slim\App($settings);

    require __DIR__ . '/src/dependencies.php';
    //$container = $app->getContainer();

    $container['errorHandler'] = function ($c) {
        return function ($request, $response, $exception) use ($c) {
            //this is wrong, i'm not with http
            return $c['response']->withStatus(500)
                ->withHeader('Content-Type', 'text/text')
                ->write($exception->getMessage());
        };
    };

    $container['notFoundHandler'] = function ($c) {
        //this is wrong, i'm not with http
        return function ($request, $response) use ($c) {
            return $c['response']
                ->withStatus(404)
                ->withHeader('Content-Type', 'text/text')
                ->write('Not Found');
        };
    };

    $app->map(['GET'], '/create-report-index', function() {
          $importer = new \Lib\ReportImporter($this->elastic);
          $res = $importer->createIndex();
          $this->logger->addInfo(serialize($res));
    });

    $app->map(['GET'], '/delete-report-index', function() {
        $importer = new \Lib\ReportImporter($this->elastic);
        $res = $importer->deleteIndex();
        $this->logger->addInfo(serialize($res));
    });

    $app->map(['GET'], '/bulk-index-report', function() {
        $importer = new \Lib\ReportImporter($this->elastic);
        $res = $importer->bulkIndex();
        $this->logger->addInfo('Bulk Index Done!!');
    });


    $app->run();
}