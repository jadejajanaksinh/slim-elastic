**How to Start App?**

`cd  slim_study
`

`composer update
`

`php -S localhost:8888 -t public index.php'
`

**Create Index**

`php cli.php /create-report-index
`

**Delete Index**

`php cli.php /delete-report-index
`

**Bulk Index**

`php cli.php /bulk-index-report
`

**Nginx Virtual Host**

`docs/nginx.conf`