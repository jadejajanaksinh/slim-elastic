<?php
error_reporting( E_ALL );
ini_set('display_errors', 'On');
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header
        'db' => [
                    'host' => 'localhost',
                    'user' => 'janak',
                    'pass' => 'janak',
                    'dbname' => 'api_platform',
                ],

        'elastic' => [
            'host' =>
                [
                    'https://elastic:XCq5mhKiAaSVofEEH0aUjTq6@3f2e3ab68c074dda81753b4d009c782a.us-east-1.aws.found.io:9243/'
                ],

        ],


        // Renderer settings
        'view' => [
            'template_path' => __DIR__ . '/../templates'
        ],
        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
    ],
];
