<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Lib\ReportFinder;


$app->get('/', function (Request $request, Response $response, array $args) {
    $response = $this->view->render($response, 'home.phtml', ['tickets' => []]);
    return $response;
});


$app->get('/report1', function (Request $request, Response $response, array $args) {
    $data = $request->getQueryParams();

    if (empty($data['page']) || $data['search'] == 'new' )    $data['page'] = 1;

    if (empty($data['keyword']) )    $data['keyword'] = '';

    $finder = new ReportFinder($this->elastic);
    $result = $finder->getDocuments($data);



    $adapter = new \Pagerfanta\Adapter\NullAdapter($result['total']);
    $pager =  new \Pagerfanta\Pagerfanta($adapter);
    $pager->setMaxPerPage(ReportFinder::PAGE_LIMIT);
    $pager->setCurrentPage($data['page']);


    $routeGenerator = function($page)  use ($data) {
        $data['page'] = $page;
        return '/report1?'.http_build_query($data);
    };

    $view = new \Pagerfanta\View\TwitterBootstrap4View();
    $options = array('proximity' => 3);
    $html = $view->render($pager, $routeGenerator, $options);

    $response = $this->view->render($response, 'report1.phtml', ['result' => $result['result'], 'html' => $html, 'param' => $data]);
    return $response;
});

$app->get('/report2', function (Request $request, Response $response, array $args) {
    $response = $this->view->render($response, 'report2.phtml', ['tickets' => []]);
    return $response;
});

$app->get('/help', function (Request $request, Response $response, array $args) {
    $response = $this->view->render($response, 'help.phtml', ['tickets' => []]);
    return $response;
});