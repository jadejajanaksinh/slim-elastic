<?php
namespace Lib;

/**
 * Interface Finder
 *
 * @author Janaksinh Jadeja
 *
 * @package Lib
 */
interface Finder
{
    function getDocuments($parameters);
}