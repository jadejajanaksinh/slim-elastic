<?php
namespace Lib;

/**
 * Interface Importer
 *
 * @author Janaksinh Jadeja
 *
 * @package Lib
 */
interface  Importer
{
    public function createIndex();

    public function deleteIndex();

    public function bulkIndex();

}