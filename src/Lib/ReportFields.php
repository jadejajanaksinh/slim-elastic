<?php
namespace Lib;

final class ReportFields
{
    const INDEX_NAME = 'report';

    const report_id = 'report_id';
    const report_code = 'report_code';
    const title = 'title';
    const pages = 'pages';
    const exhibits = 'exhibits';
    const published = 'published';
    const forecast = 'forecast';
    const publisher = 'publisher';
    const distributor = 'distributor';
    const overview = 'overview';
    const toc = 'toc';
    const companies = 'companies';
    const brochure_url = 'brochure_url';
    const price_single = 'price_single';
    const price_corporate = 'price_corporate';
    const meta_keywords = 'meta_keywords';
    const meta_description = 'meta_description';
    const type = 'type';
    const timestamp = 'timestamp';
}