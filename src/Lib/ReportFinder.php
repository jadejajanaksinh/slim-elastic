<?php
namespace Lib;
use Lib\Finder;

/**
 *
 * Class ReportFinder
 *
 * @author Janaksinh Jadeja
 * @package Lib
 */
class ReportFinder  implements Finder
{
    private $elastic;

    const PAGE_LIMIT = 30;

    /**
     * Intialize finder
     *
     * ReportFinder constructor.
     * @param $elastic
     */
    public function __construct($elastic)
    {
        $this->elastic = $elastic;
    }

    /**
     * prepare elastic search query
     *
     * @param $parameters
     * @return array
     */
    private function _filter($parameters)
    {
        $search = [
            'size' => self::PAGE_LIMIT,
            'from' => $parameters['page'] == 1 ? 0 : $parameters['page'] * self::PAGE_LIMIT,
            'query' => [
                'query_string' => [
                    'query'  => "(report_id: 5250)",
                ]
            ],
        ];

        if (!empty($parameters['order_by'])) {
            $search['sort'] = [
                $parameters['order_by'] => ['order' => isset($parameters['order_type']) && ($parameters['order_type'] == 'asc') ? 'asc' : 'desc']
            ];
        }

        return $search;
    }

    /**
     * get elastic documents
     *
     * @param $parameters
     *
     * @return mixed
     */
    public function getDocuments($parameters)
    {
        $params = [
            'index' => ReportFields::INDEX_NAME,
            'type' => 'my_type',
            'body' => $this->_filter($parameters)
        ];


        $result =  $this->elastic ->search($params);

        $data['result'] = [];
        $data['total'] = null;

        if (!empty($result['hits']) && $result['hits']['total'] > 0) {
            $data['result'] = $result['hits']['hits'];
            $data['total'] = $result['hits']['total'];
        }

        return $data;
    }


}