<?php
namespace Lib;
use Lib\ReportFields;

/**
 * Class ReportImporter
 *
 * @author Janaksinh Jadeja
 * @package Lib
 */
class  ReportImporter implements Importer
{

    public static $username;

    /**
     * @var elastic client
     */
    private $elastic;

    /**
     * Index name in elastic search
     */
    const INDEX_NAME = 'report';

    /**
     *
     * ReportImporter constructor.
     * @param $elastic
     */
    public function __construct($elastic)
    {
        $this->elastic = $elastic;
    }

    /**
     * create index
     *
     * @return mixed
     */
    public function createIndex()
    {

        $params = [
            'index' =>  ReportFields::INDEX_NAME,
            'body' => [
                'settings' => [
                    'number_of_shards' => 3,
                    'number_of_replicas' => 2
                ],
                'mappings' => [
                    'my_type' => [
                        '_source' => [
                            'enabled' => true
                        ],
                        'properties' => [
                            ReportFields::report_id => [
                                'type' => 'integer',
                            ],
                            ReportFields::report_code => [
                                'type' => 'keyword'
                            ],
                            ReportFields::title => [
                                'type' => 'keyword'
                            ],
                            ReportFields::pages => [
                                'type' => 'keyword'
                            ],
                            ReportFields::exhibits => [
                                'type' => 'keyword'
                            ],
                            ReportFields::published => [
                                'type' => 'keyword'
                            ],
                            ReportFields::forecast => [
                                'type' => 'keyword'
                            ],
                            ReportFields::publisher => [
                                'type' => 'keyword'
                            ],
                            ReportFields::distributor => [
                                'type' => 'keyword'
                            ],
                            ReportFields::overview => [
                                'type' => 'keyword'
                            ],
                            ReportFields::toc => [
                                'type' => 'keyword'
                            ],
                            ReportFields::companies => [
                                'type' => 'keyword'
                            ],
                            ReportFields::price_single => [
                                'type' => 'keyword'
                            ],
                            ReportFields::price_corporate => [
                                'type' => 'keyword'
                            ],
                            ReportFields::meta_keywords => [
                                'type' => 'keyword'
                            ],
                            ReportFields::meta_description => [
                                'type' => 'text'
                            ],
                            ReportFields::type => [
                                'type' => 'integer'
                            ],
                            ReportFields::timestamp => [
                                'type' => 'date',
                                'format' => "Y-m-d h:m:s"
                            ],

                        ]
                    ]
                ]
            ]
        ];

        return $this->elastic->indices()->create($params);
    }

    /**
     * @return mixed
     *
     * delete index from elastic search
     */
    public function deleteIndex()
    {
        $params = ['index' =>  ReportFields::INDEX_NAME];
        return $this->elastic->indices()->delete($params);
    }


    /**
     * bulk index
     *
     */
    public function bulkIndex()
    {
        $params = ['body' => []];

        for ($i = 1; $i <= 5000; $i++) {
            $params['body'][] = [
                'index' => [
                    '_index' => ReportFields::INDEX_NAME,
                    '_type' => 'my_type',
                    '_id' => $i
                ]
            ];

            $params['body'][] = [
                ReportFields::report_id => rand(0, 20000),
                ReportFields::report_code => md5(rand(1, 1000000)),
                ReportFields::title => 'title '.md5(rand(1, 1000000)),
                ReportFields::pages => 'page '.md5(rand(1, 1000000)),
                ReportFields::exhibits => 'exhibits '.md5(rand(1, 1000000)),
                ReportFields::published => 'published '.md5(rand(1, 1000000)),
                ReportFields::forecast => 'forecast '.md5(rand(1, 1000000)),
                ReportFields::publisher => 'publisher '.md5(rand(1, 1000000)),
                ReportFields::distributor => 'distributor '.md5(rand(1, 1000000)),
                ReportFields::overview => 'overview '.md5(rand(1, 1000000)),

                ReportFields::toc => 'toc '.md5(rand(1, 1000000)),
                ReportFields::companies => 'companies '.md5(rand(1, 1000000)),
                ReportFields::price_single => 'price_single '.md5(rand(1, 1000000)),
                ReportFields::price_corporate => 'price_corporate '.md5(rand(1, 1000000)),
                ReportFields::meta_keywords => 'meta_keywords '.md5(rand(1, 1000000)),
                ReportFields::meta_description => 'meta_description '.md5(rand(1, 1000000)),
                ReportFields::type => rand(0, 12),
                ReportFields::timestamp => date('Y-m-d h:m:s', strtotime('+'.rand(1, 100).' days')),
            ];

            if ($i % 1000 == 0) {
                $responses = $this->elastic->bulk($params);
                $params = ['body' => []];
                unset($responses);
            }
        }

        if (!empty($params['body'])) {
            $responses = $this->elastic->bulk($params);
        }
    }
}